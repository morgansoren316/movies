from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast
from azure.cosmosdb.table.models import Entity
from azure.cosmosdb.table.tableservice import TableService
import uuid
from dotenv import load_dotenv, find_dotenv
import os
from azure.servicebus import ServiceBusClient, ServiceBusMessage
from datetime import datetime

app = Flask(__name__)
api = Api(app)

class GetMovies(Resource):
    
    table_service: TableService = None
    
    def __init__(self):
        load_dotenv(find_dotenv())
       
        self.table_service = TableService(account_name=os.getenv("TABLES_STORAGE_ACCOUNT_NAME"), 
                                          account_key=os.getenv("TABLES_PRIMARY_STORAGE_ACCOUNT_KEY"))
    
    def get(self):
        movies = self.table_service.query_entities('moviestable', filter="PartitionKey eq 'movies'")
        movieslist = []
        for movie in movies:
            movieslist.append(movie.Name)
        return {'data': movieslist}, 200


class AddMovies(Resource):
    
    table_service: TableService = None
    
    def __init__(self):
        load_dotenv(find_dotenv())
       
        self.table_service = TableService(account_name=os.getenv("TABLES_STORAGE_ACCOUNT_NAME"), 
                                          account_key=os.getenv("TABLES_PRIMARY_STORAGE_ACCOUNT_KEY"))
    
    
    def ticks(self, dt):
        return (dt - datetime(1, 1, 1)).total_seconds() * 10000000

   
    def post(self):
        data = request.get_json()
        db_entity = Entity()
        db_entity.PartitionKey = "movies"
        db_entity.Name = data['Name']
        
        DateTimeValueForRowKey = self.ticks(datetime.utcnow())
        MaxValue = self.ticks(datetime.max)
        
        rowkey = int(MaxValue - DateTimeValueForRowKey)
        
        db_entity.RowKey = str(rowkey)
        self.table_service.insert_entity("moviestable", db_entity)
        
        movies = self.table_service.query_entities('moviestable', filter="PartitionKey eq 'movies'")
        movieslist = []
        counter = 1
        for movie in movies:
            if counter > 10:
                break
            counter = counter + 1
            movieslist.append(ServiceBusMessage(str(movie.Name)))
            
        
        
        shared_access_key = os.getenv("SHARED_ACCESS_KEY")
        queue_name = os.getenv("QUEUE_NAME")
        service_bus_name = os.getenv("SERVICE_BUS_NAME")
        
        conn = f"Endpoint=sb://{service_bus_name}/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey={shared_access_key}"
        servicebus_client = ServiceBusClient.from_connection_string(conn_str=conn, logging_enable=True)
        
        with servicebus_client:
            receiver = servicebus_client.get_queue_receiver(queue_name=queue_name, max_wait_time=5)
            with receiver:
                for msg in receiver:
                    print("Received: " + str(msg))
                    receiver.complete_message(msg)
        
        with servicebus_client:
            sender = servicebus_client.get_queue_sender(queue_name=queue_name)
            with sender:
                message = movieslist
                sender.send_messages(message)
        
        return {'data': data}, 200

api.add_resource(GetMovies, '/get')
api.add_resource(AddMovies, '/add')


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=9007)