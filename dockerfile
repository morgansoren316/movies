FROM python:3.8-buster as main



COPY .  .

RUN pip install -r requirements.txt 

RUN pip install \
        Flask \
        Flask-RESTful \
        azure-cosmosdb-table \
        python-dotenv \
        pandas \
        azure-servicebus

EXPOSE 9007
CMD ["python", "./api.py"]